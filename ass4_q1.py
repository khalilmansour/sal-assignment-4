# read from file
x = open('datafile1.txt', 'r')

def find_mean():
    # init
    sum_test_scores = 0
    for line in x:
        sum_test_scores = sum_test_scores + int(line)

    # calculate mean (divide by 10 students)
    mean_test_scores = sum_test_scores / 10

    # print
    print('The mean test score is {0}.'.format(mean_test_scores))

find_mean()

# close file
x.close()

